import { useRef } from "react";

const UncontrolledCounter = () => {
    let contador = 0;
    const spanRef = useRef();

    const aumentarHandler = () => {
        contador = ++contador;
        spanRef.current.span = contador;
        console.log(spanRef.current.span);
    };

    const reiniciarHandler = () => {
        contador = 0;
        spanRef.current.span = contador;
        console.log(spanRef.current.span);
    };

    const disminuirHandler = () => {
        contador = --contador;
        spanRef.current.span = contador;
        console.log(spanRef.current.span);
    };

    return (
        <div className="container">
            <h1>Componente "UncontrolledCounter"</h1>
            <div className="container-counter">
                <strong>Valor: </strong> <span value={contador} ref={spanRef}> {contador} </span>
                <br />
            </div>
            <div className="container-btn">
                <button onClick={aumentarHandler} className="btn-aumentar">Aumentar</button>
                <button onClick={reiniciarHandler}><i className="fa fa-refresh"></i></button>
                <button onClick={disminuirHandler} className="btn-disminuir">Disminuir</button>
            </div>
        </div>
    );
}

export default UncontrolledCounter;