import { useState } from "react";

const ControlledCounter = () => {
    let [contador, setContador] = useState(0);

    const aumentarHandler = () => {
        setContador(++contador);
    };

    const reiniciarHandler = () => {
        setContador(0);
    };

    const disminuirHandler = () => {
        setContador(--contador);
    };

    const mostrarTipoDeValor = () => {
        if (contador === 0) {
            return <strong>-</strong>
        } else {
            return contador > 0 ? <strong>Número positivo</strong> : <strong>Número negativo</strong>
        }
    };

    return (
        <div className="container">
            <h1>Componente "ControlledCounter"</h1>
            <div className="container-counter">
                <strong>Valor: </strong> {contador}
                <br />
                {mostrarTipoDeValor()}
            </div>
            <div className="container-btn">
                <button onClick={aumentarHandler} className="btn-aumentar">Aumentar</button>
                <button onClick={reiniciarHandler}><i className="fa fa-refresh"></i></button>
                <button onClick={disminuirHandler} className="btn-disminuir">Disminuir</button>
            </div>
        </div>
    );
}
export default ControlledCounter;