import { useRef } from "react";

const UncontrolledForm = () => {

  const inputRef = useRef(null);

  const submitHandler = () => {
    if (esNombreVacio(inputRef)) {
      alert("El campo nombre es obligatorio.");
      inputRef.current.focus();
    } else if (esNombreInvalido(inputRef)) {
      alert("El campo nombre no puede contener dígitos.");
    } else {
      alert("Hola! " + inputRef.current.value);
    }
  };

  function esNombreInvalido(inputRef) {
    return inputRef.current.value.match(/^\d+$/);
  }

  function esNombreVacio(inputRef) {
    return !inputRef.current.value;
  }

  return (
    <div className="container">
      <h1>Componente "UncontrolledForm"</h1>
      <div className="container-form">
        <input placeholder="Ingresá tu nombre" type="text" ref={inputRef} />
        <button className="btn-submit-form" onClick={submitHandler}>Submit</button>
      </div>
    </div>
  );
}

export default UncontrolledForm;

