import * as React from "react";
import './index.css'
import ControlledCounter from "./components/counter/ControlledCounter";
import UncontrolledCounter from "./components/counter/UncontrolledCounter";
import UncontrolledForm from "./components/form/UncontrolledForm";

const App = () => {
  return (
    <div>
      <ControlledCounter></ControlledCounter>
      <hr/>
      <UncontrolledCounter></UncontrolledCounter>
      <hr/>
      <UncontrolledForm></UncontrolledForm>
    </div>
  )
}
export default App;